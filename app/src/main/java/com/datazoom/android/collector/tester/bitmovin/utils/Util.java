package com.datazoom.android.collector.tester.bitmovin.utils;

import android.content.Context;
import android.widget.Toast;

public class Util {
    public static void showToastShort(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
